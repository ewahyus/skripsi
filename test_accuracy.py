# -*- coding: utf-8 -*-

from __future__ import print_function, division

import torch
import torch.nn as nn
import torch.optim as optim
from torch.optim import lr_scheduler
from torch.autograd import Variable
import numpy as np
import torchvision
from torchvision import datasets, models, transforms
import time
import os

data_transforms = {
    'data': transforms.Compose([
        transforms.Scale(256),
        transforms.CenterCrop(224),
        transforms.ToTensor(),
        transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
    ]),
}
LOG_FILE = "log_test_cnn2.txt"
data_dir = 'data_testing'
image_datasets = {x: datasets.ImageFolder(os.path.join(data_dir, x),
                                          data_transforms[x])
                  for x in ['data']}
dataloaders = {x: torch.utils.data.DataLoader(image_datasets[x], batch_size=4,
                                             shuffle=True, num_workers=4)
              for x in ['data']}
dataset_sizes = {x: len(image_datasets[x]) for x in ['data']}
class_names = image_datasets['data'].classes

use_gpu = torch.cuda.is_available()

######################################################################
# Training the model
# ------------------
#
# Now, let's write a general function to train a model. Here, we will
# illustrate:
#
# -  Scheduling the learning rate
# -  Saving the best model
#
# In the following, parameter ``scheduler`` is an LR scheduler object from
# ``torch.optim.lr_scheduler``.


def evaluate_model(model, criterion):
    since = time.time()

    best_acc = 0.0

    # Each epoch has a training and validation phase
    for phase in ['data']:

        running_loss = 0.0
        running_corrects = 0
        idx = 1
        print(dataset_sizes[phase], 'size')
        # Iterate over data.
        for data in dataloaders[phase]:
            # get the inputs
            inputs, labels = data

            # wrap them in Variable
            if use_gpu:
                inputs = Variable(inputs.cuda())
                labels = Variable(labels.cuda())
            else:
                inputs, labels = Variable(inputs), Variable(labels)

            print(len(inputs))
            print('Processing', idx)
            idx += 1

            # forward
            outputs = model(inputs)
            _, preds = torch.max(outputs.data, 1)
            loss = criterion(outputs, labels)
            # statistics
            running_loss += loss.data[0]
            running_corrects += torch.sum(preds == labels.data)


        loss = running_loss / dataset_sizes[phase]
        acc = running_corrects / dataset_sizes[phase]

        time_elapsed = time.time() - since
        print('total corrects: ', running_corrects)
        print('total data: ', dataset_sizes[phase])
        
        print('loss: ', loss, 'acc: ', acc)
        print('Testing complete in {:.0f}m {:.0f}s'.format(
            time_elapsed // 60, time_elapsed % 60))
        
        text = str(phase) + ' Acc: ' + str(acc) + '\n'
        with open(LOG_FILE, 'a') as f:
            f.write(text)



######################################################################
# ConvNet as fixed feature extractor
# ----------------------------------
#
# Here, we need to freeze all the network except the final layer. We need
# to set ``requires_grad == False`` to freeze the parameters so that the
# gradients are not computed in ``backward()``.
#
# You can read more about this in the documentation
# `here <http://pytorch.org/docs/notes/autograd.html#excluding-subgraphs-from-backward>`__.
#

model_conv = models.resnet152(pretrained=True)
num_ftrs = model_conv.fc.in_features
model_conv.fc = nn.Linear(num_ftrs, 10)
model_conv.load_state_dict(torch.load("weightnew"))

criterion = nn.CrossEntropyLoss()

evaluate_model(model_conv, criterion)