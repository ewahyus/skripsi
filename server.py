from __future__ import print_function, division
from flask import Flask, url_for, send_from_directory, request,jsonify,current_app
import logging, os
from werkzeug import secure_filename
from flask_cors import CORS,cross_origin
from functools import update_wrapper
from datetime import timedelta

import torch
import torch.nn as nn
from torch.optim import lr_scheduler
from torch.autograd import Variable
import torchvision
from torchvision import datasets, models, transforms
from PIL import Image

class_names = [
        'elektronik', 
        'fashion'
    ]

use_gpu = torch.cuda.is_available()



DATA_DIR = "ft_extract/full"

preprocess = transforms.Compose([
    transforms.Scale(256),
    transforms.CenterCrop(224),
    transforms.ToTensor(),
    transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
])


app = Flask(__name__)
cors = CORS(app)
# cors.config('CORS_HEADERS') = 'Content-Type'

file_handler = logging.FileHandler('server.log')
app.logger.addHandler(file_handler)
app.logger.setLevel(logging.INFO)

PROJECT_HOME = os.path.dirname(os.path.realpath(__file__))
UPLOAD_FOLDER = '{}/uploads/'.format(PROJECT_HOME)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
IMG_FOLDER = '{}/ft_extract/full'.format(PROJECT_HOME)
app.config['IMG_FOLDER'] = IMG_FOLDER

def init_model(feature_extractor):
    model = models.resnet152(pretrained=False)
    num_ftrs = model.fc.in_features
    model.fc = nn.Linear(num_ftrs, 10)
    model.load_state_dict(torch.load("weightnew"))

    if feature_extractor:
        modules = list(model.children())[:-1]
        model_feature_extractor = nn.Sequential(*modules)
        if use_gpu:
            model_feature_extractor = model_feature_extractor.cuda()
        return model_feature_extractor
    if use_gpu:
        model = mode.cuda()
    return model


def create_new_folder(local_dir):
    newpath = local_dir
    if not os.path.exists(newpath):
        os.makedirs(newpath)
    return newpath
   

@app.route('/', methods = ['POST'])
@cross_origin()
def api_root():
    app.logger.info(PROJECT_HOME)
    if request.method == 'POST' and request.files['file']:
        app.logger.info(app.config['UPLOAD_FOLDER'])
        img = request.files['file']
        img_name = secure_filename(img.filename)
        create_new_folder(app.config['UPLOAD_FOLDER'])
        saved_path = os.path.join(app.config['UPLOAD_FOLDER'], img_name)
        app.logger.info("saving {}".format(saved_path))
        img.save(saved_path)

        input = image_loader(saved_path)
        if input.size()[1] != 3:
            return jsonify({
                'status' : 'Dimensi gambar tidak sesuai'
                })
        #print(input.size()[1])
        predicted = predict_category(input)
        similar = find_similar(input, predicted)
        return jsonify({
               'category' : predicted,
               'img' : similar
        })

    else:
        return "Where is the image?"
	
def predict_category(input):
    model = init_model(feature_extractor=False)
    output = model(input)
    _,pred = torch.max(output.data, 1)
    return class_names[pred[0]]


def find_similar(inputQuery, category):
    model_feature_extractor = init_model(feature_extractor=True)
    query_embedding = model_feature_extractor(inputQuery)
    
    directory = os.path.join(DATA_DIR, category) 
    smallest = 3
    img_list = []
    result_list = []
    delta = 0.1
    if(category == 'elektronik'):
        delta = 0.2
    treshhold = 0
    i = 0 
    for filename in os.listdir(directory):
        if filename.endswith(".jpg") or filename.endswith(".png"): 
            filename = os.path.join(directory, filename)

            input = image_loader(filename)
            input_size = input.size()
            if input_size[1] == 3:
                image_embedding = model_feature_extractor(input)
                dist = torch.dist(query_embedding, image_embedding)
                dist = dist.data.numpy()
                if smallest > dist:
                    smallest = dist
                container = {
                    "dist": dist.tolist(),
                    "filename": filename
                }
                img_list.append(container)
        i += 1
    
    treshhold = smallest + delta

    for index, item in enumerate(img_list):
        if (img_list[index]['dist'] >= smallest) and (img_list[index]['dist'] <= treshhold):
            result = {
                "dist" : img_list[index]['dist'],
                "filename" : img_list[index]['filename']
            }
            result_list.append(result)


    return result_list
    




def image_loader(image_name):
    """load image, returns cuda tensor"""
    image = Image.open(image_name)
    image = preprocess(image).float()
    image = image.unsqueeze(0)
    if use_gpu:
        image = Variable(image.cuda())
    else:
        image = Variable(image)
    return image

@app.route('/files/<path:path>')
def get_file(path):
    """Download a file."""
    return send_from_directory(app.config['IMG_FOLDER'],path, as_attachment=True)
    #return send_from_directory(DATA_DIR, path, as_attachment=True) 



if __name__ == '__main__':
    app.run(port=5000, debug=True)
